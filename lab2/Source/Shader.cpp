#include "ITPEnginePCH.h"
#include <SDL/SDL_log.h>

Shader::Shader(class Game& game)
	:Asset(game)
	,mGraphicsDriver(mGame.GetRenderer().GetGraphicsDriver())
{
}

Shader::~Shader()
{
}

void Shader::SetActive()
{
	// TODO: Lab 2
	// Set this shader as active, and set
	// any constant buffers
	mGraphicsDriver.SetVertexShader(mVertexShader);
	mGraphicsDriver.SetPixelShader(mPixelShader);
	mGraphicsDriver.SetVSConstantBuffer(mPerCameraBuffer, 0);
	mGraphicsDriver.SetVSConstantBuffer(mPerObjectBuffer, 1);
	mGraphicsDriver.SetPSConstantBuffer(mPerCameraBuffer, 0);
	mGraphicsDriver.SetPSConstantBuffer(mPerObjectBuffer, 1);
	mGraphicsDriver.SetPSSamplerState(mDefaultSampler, 0);
	// TODO: Lab 3
	// Set the lighting constant buffer

	// TODO: Lab 4
	// Set the matrix palette buffer, if it exists
}

void Shader::CreateMatrixPaletteBuffer()
{
	// TODO: Lab 4
}

void Shader::UploadPerCameraConstants()
{
	// TODO: Lab 2
	memcpy(mGraphicsDriver.MapBuffer(mPerCameraBuffer), &mPerCamera, sizeof(mPerCamera));
	mGraphicsDriver.UnmapBuffer(mPerCameraBuffer);
}

void Shader::UploadPerObjectConstants()
{
	// TODO: Lab 2
	memcpy(mGraphicsDriver.MapBuffer(mPerObjectBuffer), &mPerObject, sizeof(mPerObject));
	mGraphicsDriver.UnmapBuffer(mPerObjectBuffer);
}

void Shader::UploadLightingConstants()
{
	// TODO: Lab 3
}

void Shader::UploadMatrixPalette(const struct MatrixPalette& palette)
{
	// TODO: Lab 4
}

void Shader::BindTexture(TexturePtr texture, int slot)
{
	texture->SetActive(slot);
}

bool Shader::Load(const char* fileName, class AssetCache* cache)
{
	// TODO: Lab 2
	
	if (mGraphicsDriver.CompileShaderFromFile(fileName, "VS", "vs_4_0", mCompiledVS))
	{
		mVertexShader = mGraphicsDriver.CreateVertexShader(mCompiledVS);
		mGraphicsDriver.SetVertexShader(mVertexShader);
		mPerCameraBuffer = mGraphicsDriver.CreateGraphicsBuffer(&mPerCamera, sizeof(mPerCamera), EBF_ConstantBuffer, ECPUAF_CanWrite, EGBU_Dynamic);
		mPerObjectBuffer = mGraphicsDriver.CreateGraphicsBuffer(&mPerObject, sizeof(mPerObject), EBF_ConstantBuffer, ECPUAF_CanWrite, EGBU_Dynamic);
		mGraphicsDriver.SetVSConstantBuffer(mPerCameraBuffer, 0);
		mGraphicsDriver.SetVSConstantBuffer(mPerObjectBuffer, 1);
		mDefaultSampler = mGraphicsDriver.CreateSamplerState();
		mGraphicsDriver.SetPSSamplerState(mDefaultSampler, 0);
	}
	else
	{
		return false;
	}
	if (mGraphicsDriver.CompileShaderFromFile(fileName, "PS", "ps_4_0", mCompiledPS))
	{
		mPixelShader = mGraphicsDriver.CreatePixelShader(mCompiledPS);
		mGraphicsDriver.SetPixelShader(mPixelShader);
		mPerCameraBuffer = mGraphicsDriver.CreateGraphicsBuffer(&mPerCamera, sizeof(mPerCamera), EBF_ConstantBuffer, ECPUAF_CanWrite, EGBU_Dynamic);
		mPerObjectBuffer = mGraphicsDriver.CreateGraphicsBuffer(&mPerObject, sizeof(mPerObject), EBF_ConstantBuffer, ECPUAF_CanWrite, EGBU_Dynamic);
		mGraphicsDriver.SetPSConstantBuffer(mPerCameraBuffer, 0);
		mGraphicsDriver.SetPSConstantBuffer(mPerObjectBuffer, 1);
		mDefaultSampler = mGraphicsDriver.CreateSamplerState();
		mGraphicsDriver.SetPSSamplerState(mDefaultSampler, 0);
	}
	else
	{
		return false;
	}
	return true;
}
