// We want to use row major matrices
#pragma pack_matrix(row_major)

// TODO: Lab 2
cbuffer CameraConstants: register(b0)
{
	float4x4 viewProjectionMatrix;
}

cbuffer CameraConstants : register(b1)
{
	float4x4 worldViewMatrix;
}
SamplerState DefaultSampler : register(s0);
Texture2D DiffuseTexture : register(t0);