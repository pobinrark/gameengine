// We want to use row major matrices
#pragma pack_matrix(row_major)

// TODO: Lab 2
struct PointLightData {
	float3 diffuseColor;
	float3 specularColor;
	float3 position;

	float specularPower;
	float innerRadius;
	float outerRadius;

	bool isEnabled;
};
cbuffer CameraConstants: register(b0)
{
	float4x4 viewProjectionMatrix;
	float3 worldSpacePosition;
}

cbuffer CameraConstants : register(b1)
{
	float4x4 worldViewMatrix;
}
cbuffer CameraConstants : register(b2)
{
	float3 ambientLight;
	PointLightData mPointLightData[8];
}

SamplerState DefaultSampler : register(s0);
Texture2D DiffuseTexture : register(t0);
