#include "Constants.hlsl"

// Input structs for vertex and pixel shader
struct VS_INPUT
{
	float3 mPos : POSITION;
	float3 mNorm : NORMAL;
	float2 mTex : TEXCOORD0;
};

struct PS_INPUT
{
	float4 mPos : SV_POSITION;
	float2 mTex: TEXCOORD0;
	float3 mWorldSpaceNormal : NORMAL;
	float3 mWorldSpacePosition : POSITION;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
PS_INPUT VS(VS_INPUT input)
{
	PS_INPUT output;

	// TODO: Lab 2
	// (For now, just directly copy position/tex coord)
	output.mPos = float4(input.mPos, 1.0f);
	output.mTex = input.mTex;
	output.mWorldSpaceNormal = mul(float4(input.mNorm, 1.0f), worldViewMatrix).xyz;
	output.mWorldSpacePosition = mul(float4(input.mPos, 1.0f), worldViewMatrix).xyz;
	output.mPos = mul(output.mPos, worldViewMatrix);
	output.mPos = mul(output.mPos, viewProjectionMatrix);
	return output;
}


//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(PS_INPUT input) : SV_Target
{
	// TODO: Lab 3
	float3 Phong = ambientLight;
	float3 N = normalize(input.mWorldSpaceNormal);
	for (int i = 0; i < 8; i++)
	{
		if (mPointLightData[i].isEnabled)
		{
			float3 L = normalize(mPointLightData[i].position - input.mWorldSpacePosition);
			float3 V = normalize(worldSpacePosition - input.mWorldSpacePosition);
			if (dot(N, L) > 0)
			{
				float mDistance = distance(mPointLightData[i].position, input.mWorldSpacePosition);
				float mSmoothStep = smoothstep(mPointLightData[i].innerRadius, mPointLightData[i].outerRadius, mDistance);

				float3 SpecularColor = lerp(mPointLightData[i].specularColor, float3(0.0f, 0.0f, 0.0f), mSmoothStep);
				float3 R = reflect(-L, N);

				Phong += lerp(mPointLightData[i].diffuseColor, float3(0.0f, 0.0f, 0.0f), mSmoothStep) * dot(N, L);
				Phong += SpecularColor * pow(max(0.0f, dot(R, V)), mPointLightData[i].specularPower);
				//Phong += mPointLightData[i].diffuseColor * dot(N, L);
			}
		}
	}
	Phong = saturate(Phong);
	return DiffuseTexture.Sample(DefaultSampler, input.mTex) * float4(Phong, 1.0f);   // Yellow, with Alpha = 1

}
