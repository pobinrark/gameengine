#include "ITPEnginePCH.h"

Matrix4 BoneTransform::ToMatrix() const
{
	// TODO: Lab 4
	return Matrix4::Identity * Matrix4::CreateFromQuaternion(mRotation) * Matrix4::CreateTranslation(mTranslation);
}

BoneTransform Interpolate(const BoneTransform& a, const BoneTransform& b, float f)
{
	// TODO: Lab 4
	Quaternion rotation = Slerp(a.mRotation, b.mRotation, f);
	Vector3 translation = Lerp(a.mTranslation, b.mTranslation, f);
	BoneTransform t;
	t.mRotation = rotation;
	t.mTranslation = translation;
	return t;
}
