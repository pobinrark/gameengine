#include "ITPEnginePCH.h"

Renderer::Renderer(Game& game)
	:mGame(game)
	,mWindow(nullptr)
	,mWidth(0)
	,mHeight(0)
{

}

Renderer::~Renderer()
{
	// Clear components...
	mDrawComponents.clear();
	mComponents2D.clear();

	mDepthBuffer.reset();
	mSpriteDepthState.reset();
	mMeshDepthState.reset();
	mRasterState.reset();

	mSpriteBlendState.reset();
	mMeshBlendState.reset();

	mSpriteShader.reset();
	mSpriteVerts.reset();

	mMeshShaders.clear();

	// Shutdown the input cache and graphics driver
	mInputLayoutCache.reset();
	mGraphicsDriver.reset();

	if (mWindow != nullptr)
	{
		SDL_DestroyWindow(mWindow);
	}
}

bool Renderer::Init(int width, int height)
{
	// Create our SDL window
	mWindow = SDL_CreateWindow("ITP Engine 2 Demo!", 100, 100, width, height, 
		0);

	if (!mWindow)
	{
		SDL_Log("Could not create window.");
		return false;
	}

	mGraphicsDriver = std::make_shared<GraphicsDriver>(GetActiveWindow());
	mInputLayoutCache = std::make_shared<InputLayoutCache>();

	mWidth = width;
	mHeight = height;

	if (!InitFrameBuffer())
	{
		return false;
	}

	if (!InitShaders())
	{
		return false;
	}

	if (!InitSpriteVerts())
	{
		return false;
	}

	return true;
}

void Renderer::RenderFrame()
{
	Clear();
	DrawComponents();
	Present();
}

void Renderer::AddComponent(DrawComponentPtr component)
{
	if (IsA<SpriteComponent>(component) || IsA<FontComponent>(component))
	{
		mComponents2D.emplace(component);
	}
	else
	{
		mDrawComponents.emplace(component);
	}
}

void Renderer::RemoveComponent(DrawComponentPtr component)
{
	if (IsA<SpriteComponent>(component) || IsA<FontComponent>(component))
	{
		auto iter = mComponents2D.find(component);
		if (iter != mComponents2D.end())
		{
			mComponents2D.erase(iter);
		}
	}
	else
	{
		auto iter = mDrawComponents.find(component);
		if (iter != mDrawComponents.end())
		{
			mDrawComponents.erase(iter);
		}
	}
}

void Renderer::AddPointLight(PointLightComponentPtr light)
{
	// TODO: Lab 3
	mPointLights.emplace(light);
	UpdatePointLights();
}

void Renderer::RemovePointLight(PointLightComponentPtr light)
{
	// TODO: Lab 3
	mPointLights.erase(light);
	UpdatePointLights();
}

void Renderer::UpdatePointLights()
{
	// TODO: Lab 3
	int i = 0;
	for (auto& mesh : mMeshShaders)
	{
		for (auto& light : mPointLights)
		{
			if (light == nullptr)
			{
				mesh.second->GetLightingConstants().mPointLightData[i].isEnabled = false;
			}
			else
			{
				mesh.second->GetLightingConstants().mPointLightData[i] = light->GetData();
			}
			i++;
		}
		mesh.second->UploadLightingConstants();
		i = 0;
	}
}

void Renderer::DrawSprite(TexturePtr texture, const Matrix4& worldTransform)
{
 	// TODO: Lab 2
	mSpriteShader->SetActive();
	mSpriteShader->GetPerObjectConstants().worldViewMatrix = worldTransform;
	mSpriteShader->UploadPerObjectConstants();
	mSpriteShader->BindTexture(texture, 0);
	DrawVertexArray(mSpriteVerts);
}

void Renderer::DrawMesh(VertexArrayPtr vertArray, TexturePtr texture, const Matrix4& worldTransform, EMeshShader type /*= EMS_Basic*/)
{
	// TODO: Lab 2
	/*for (auto& mMesh : mMeshShaders)
	{

		mMesh.second->SetActive();
		mMesh.second->GetPerObjectConstants().worldViewMatrix = worldTransform;
		mMesh.second->UploadPerObjectConstants();
		mMesh.second->BindTexture(texture, 0);
		DrawVertexArray(vertArray);
	}*/
	mMeshShaders.find(type)->second->SetActive();
	mMeshShaders.find(type)->second->GetPerObjectConstants().worldViewMatrix = worldTransform;
	mMeshShaders.find(type)->second->UploadPerObjectConstants();
	mMeshShaders.find(type)->second->BindTexture(texture, 0);
	DrawVertexArray(vertArray);
}

void Renderer::DrawSkeletalMesh(VertexArrayPtr vertArray, TexturePtr texture, const Matrix4& worldTransform, const struct MatrixPalette& palette)
{
	// TODO: Lab 4
	mMeshShaders.find(EMS_Skinned)->second->SetActive();
	mMeshShaders.find(EMS_Skinned)->second->GetPerObjectConstants().worldViewMatrix = worldTransform;
	mMeshShaders.find(EMS_Skinned)->second->UploadPerObjectConstants();
	mMeshShaders.find(EMS_Skinned)->second->BindTexture(texture, 0);
	mMeshShaders.find(EMS_Skinned)->second->UploadMatrixPalette(palette);
	DrawVertexArray(vertArray);
}

void Renderer::DrawVertexArray(VertexArrayPtr vertArray)
{
	// TODO: Lab 2
	vertArray->SetActive();
	mGraphicsDriver->DrawIndexed(vertArray->GetIndexCount(), 0, 0);
}

void Renderer::UpdateViewMatrix(const Matrix4& view)
{
	// TODO: Lab 2
	mView = view;
	for (auto &emShader : mMeshShaders)
	{
		emShader.second->GetPerCameraConstants().viewProjectionMatrix = mView * mProj;
		emShader.second->GetPerCameraConstants().worldSpacePosition = emShader.second->GetPerCameraConstants().viewProjectionMatrix.GetTranslation();
	}
}

void Renderer::SetAmbientLight(const Vector3& color)
{
	// TODO: Lab 3
	for (auto & mMesh : mMeshShaders)
	{
		mMesh.second->GetLightingConstants().ambientLight = color;
		mMesh.second->UploadLightingConstants();
	}
}

Vector3 Renderer::Unproject(const Vector3& screenPoint) const
{
	// Convert screenPoint to device coordinates (between -1 and +1)
	Vector3 deviceCoord = screenPoint;
	deviceCoord.x /= (mWidth) * 0.5f;
	deviceCoord.y /= (mHeight) * 0.5f;

	// First, undo the projection
	Matrix4 unprojection = mProj;
	unprojection.Invert();
	Vector3 unprojVec = TransformWithPerspDiv(deviceCoord, unprojection);

	// Now undo the view matrix
	Matrix4 uncamera = mView;
	uncamera.Invert();
	return Transform(unprojVec, uncamera);
}

void Renderer::Clear()
{
	// TODO: Lab 2
	mGraphicsDriver->ClearBackBuffer(Vector3(0.0f, 0.0f, 0.0f), 1.0f);
	mGraphicsDriver->ClearDepthStencil(mDepthBuffer, 1.0f);

}

void Renderer::DrawComponents()
{
	// TODO: Lab 2
	// Enable depth buffering and disable blending
	mGraphicsDriver->SetDepthStencilState(mMeshDepthState);
	mGraphicsDriver->SetBlendState(mMeshBlendState);
	// TODO: Lab 2
	// Upload per camera constants
	for (auto &mShader : mMeshShaders)
	{
		mShader.second->UploadPerCameraConstants();

	}
	// TODO: Lab 3
	// Update point lights
	UpdatePointLights();

	// Draw the normal components
	for (auto& comp : mDrawComponents)
	{
		if (comp->IsVisible())
		{
			comp->Draw(*this);
		}
	}

	// TODO: Lab 2
	// Disable depth buffering and enable blending
	mGraphicsDriver->SetDepthStencilState(mSpriteDepthState);
	mGraphicsDriver->SetBlendState(mSpriteBlendState);
	// Draw the 2D components
	for (auto& comp : mComponents2D)
	{
		if (comp->IsVisible())
		{
			comp->Draw(*this);
		}
	}
}

void Renderer::Present()
{
	// TODO: Lab 2
	mGraphicsDriver->Present();
}

bool Renderer::InitFrameBuffer()
{
	// TODO: Lab 2
	mRasterState = mGraphicsDriver->CreateRasterizerState(EFM_Solid);
	mGraphicsDriver->SetRasterizerState(mRasterState);

	mDepthBuffer = mGraphicsDriver->CreateDepthStencil(mWidth, mHeight);
	mGraphicsDriver->SetDepthStencil(mDepthBuffer);

	mMeshDepthState = mGraphicsDriver->CreateDepthStencilState(true, ECF_Less);
	mSpriteDepthState = mGraphicsDriver->CreateDepthStencilState(false, ECF_Always);

	mMeshBlendState = mGraphicsDriver->CreateBlendState(false);
	mSpriteBlendState = mGraphicsDriver->CreateBlendState(true);
	return true;
}

bool Renderer::InitShaders()
{
	// TODO: Lab 2
	// Load sprite shader and basic shader
	// And create input layouts needed for both
	mSpriteShader = mGame.GetAssetCache().Load<Shader>("Shaders/Sprite.hlsl");
	if (mSpriteShader == NULL)
	{
		return false;
	}
	InputLayoutElement elements[] = {
		InputLayoutElement("POSITION", 0, EGF_R32G32B32_Float, 0),
		InputLayoutElement("TEXCOORD", 0, EGF_R32G32_Float, 12),
	};
	mInputLayoutCache->RegisterLayout("positiontexcoord", mGraphicsDriver->CreateInputLayout(elements, 2, mSpriteShader->GetCompiledVS()));
	mSpriteShader->GetPerCameraConstants().viewProjectionMatrix = Matrix4::CreateOrtho(static_cast<float>(mWidth), static_cast<float>(mHeight),
		1000.0f, -1000.0f);

	mSpriteShader->UploadPerCameraConstants();
	mProj = Matrix4::CreatePerspectiveFOV(Math::ToRadians(70.0f),
		static_cast<float>(mWidth),
		static_cast<float>(mHeight),
		25.0f,
		10000.0f);
	InputLayoutElement elements2[] = {
		InputLayoutElement("POSITION", 0, EGF_R32G32B32_Float, 0),
		InputLayoutElement("NORMAL", 0, EGF_R32G32B32_Float, 12),
		InputLayoutElement("TEXCOORD", 0, EGF_R32G32_Float, 24)
	};
	ShaderPtr mBasicMesh = mGame.GetAssetCache().Load<Shader>("Shaders/BasicMesh.hlsl");
	if (mBasicMesh != nullptr)
	{
		mMeshShaders.emplace(EMS_Basic, mBasicMesh);

		mInputLayoutCache->RegisterLayout("positionnormaltexcoord",
			mGraphicsDriver->CreateInputLayout(elements2, 3, mBasicMesh->GetCompiledVS()));
	}
	// TODO: Lab 3
	// Load phong shader
	ShaderPtr mPhong = mGame.GetAssetCache().Load<Shader>("Shaders/Phong.hlsl");
	mPhong->GetPerCameraConstants().viewProjectionMatrix = Matrix4::CreateOrtho(static_cast<float>(mWidth), static_cast<float>(mHeight),
		1000.0f, -1000.0f);
	InputLayoutElement elements3[] = {
		InputLayoutElement("POSITION", 0, EGF_R32G32B32_Float, 0),
		InputLayoutElement("NORMAL", 0, EGF_R32G32B32_Float, 12),
		InputLayoutElement("TEXCOORD", 0, EGF_R32G32_Float, 24)
	};
	if (mPhong != nullptr)
	{
		mMeshShaders.emplace(EMS_Phong, mPhong);
		mInputLayoutCache->RegisterLayout("positionnormaltexcoord",
			mGraphicsDriver->CreateInputLayout(elements3, 3, mMeshShaders.at(EMS_Phong)->GetCompiledVS()));
	}

	// TODO: Lab 4
	// Load skinned shader and create appropriate input layout
	ShaderPtr mSkin = mGame.GetAssetCache().Load<Shader>("Shaders/Skinned.hlsl");
	mSkin->GetPerCameraConstants().viewProjectionMatrix = Matrix4::CreateOrtho(static_cast<float>(mWidth), static_cast<float>(mHeight),
		1000.0f, -1000.0f);
	mSkin->SetActive();
	InputLayoutElement elements4[] = {
		InputLayoutElement("POSITION", 0, EGF_R32G32B32_Float, 0),
		InputLayoutElement("NORMAL", 0, EGF_R32G32B32_Float, 12),
		InputLayoutElement("BONE", 0, EGF_R8G8B8A8_UInt, 24),
		InputLayoutElement("WEIGHT", 0, EGF_R8G8B8A8_UNorm, 28),
		InputLayoutElement("TEXCOORD", 0 ,EGF_R32G32_Float, 32)
	};
	if (mSkin != nullptr)
	{
		mMeshShaders.emplace(EMS_Skinned, mSkin);
		mInputLayoutCache->RegisterLayout("positionnormalbonesweightstexcoord",
			mGraphicsDriver->CreateInputLayout(elements4, 5, mMeshShaders.at(EMS_Skinned)->GetCompiledVS()));
		mSkin->CreateMatrixPaletteBuffer();
	}
	return true;
}

bool Renderer::InitSpriteVerts()
{
	// Create the vertex array for sprites
	float verts[] =
	{
		-0.5f, 0.5f, 0.0f, 0.0f, 0.0f,  // top left
		0.5f, 0.5f, 0.0f, 1.0f, 0.0f,   // top right
		0.5f, -0.5f, 0.0f, 1.0f, 1.0f,  // bottom right
		-0.5f, -0.5f, 0.0f, 0.0f, 1.0f, // bottom left
	};

	uint16_t indices[] =
	{
		0, 1, 2, // <top left, top right, bottom right>
		2, 3, 0, // <bottom right, bottom left, top left>
	};

	mSpriteVerts = VertexArray::Create(GetGraphicsDriver(), GetInputLayoutCache(),
		verts, 4, 20, "positiontexcoord", indices, 6);

	return true;
}
