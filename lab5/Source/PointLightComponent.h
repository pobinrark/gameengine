#pragma once
#include "Component.h"
#include "PointLightData.h"

class PointLightComponent : public Component
{
	DECL_COMPONENT(PointLightComponent, Component);
public:
	PointLightComponent(Actor& owner);

	void Register() override;
	void Unregister() override;

	void OnUpdatedTransform() override;

	const PointLightData& GetData() const { return mData; }

	// TODO: Lab 3
	// Add getter/setter functions for PointLightData
	Vector3 GetDiffuseColor() { return mData.diffuseColor; }
	Vector3 GetSpecularColor() { return mData.specularColor; }
	Vector3 GetPosition() { return mData.position; }
	float GetSpecularPower() { return mData.specularPower; }
	float GetInnerRadius() { return mData.innerRadius; }
	float GetOuterRadius() { return mData.outerRadius; }
	bool isEnabled() { return mData.isEnabled; }

	void SetDiffuseColor(Vector3 color) { mData.diffuseColor = color; }
	void SetSpecularColor(Vector3 color) { mData.specularColor = color; }
	void SetPosition(Vector3 pos) { mData.position = pos; }
	void SetSpecularPower(float power) { mData.specularPower = power; }
	void SetInnerRadius(float radius) { mData.innerRadius = radius; }
	void SetOuterRadius(float radius) { mData.outerRadius = radius; }
	void SetEnabled(bool enabled) { mData.isEnabled = enabled; }
	void SetProperties(const rapidjson::Value& properties) override;
private:
	PointLightData mData;
};

DECL_PTR(PointLightComponent);
