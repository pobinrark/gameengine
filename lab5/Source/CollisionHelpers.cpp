#include "ITPEnginePCH.h"

namespace Collision
{

	bool Intersects(const Sphere& a, const Sphere& b)
	{
		Vector3 diff = a.mCenter - b.mCenter;
		float DistSq = diff.LengthSq();
		float sumRadiiSq = (a.mRadius + b.mRadius) * (a.mRadius + b.mRadius);
		if (DistSq <= sumRadiiSq)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	bool Intersects(const AxisAlignedBox & a, const AxisAlignedBox & b)
	{
		// TODO: Lab 5
		// For now, it never intersects
		if (a.mMin.x > b.mMax.x)
			return false;
		if (a.mMin.y > b.mMax.y)
			return false;
		if (a.mMin.z > b.mMax.z)
			return false;

		if (b.mMin.x > a.mMax.x)
			return false;
		if (b.mMin.y > a.mMax.y)
			return false;
		if (b.mMin.z > a.mMax.z)
			return false;
		
		return true;
	}

	bool SegmentCast(const LineSegment& segment, const AxisAlignedBox& box, Vector3& outPoint)
	{
		// TODO: Lab 5
		// For now, it never intersects
		float tmin = 0.0f;
		
		Vector3 d = segment.mEnd - segment.mStart;
		float tmax = d.Length();
		d.Normalize();
		Vector3 start = segment.mStart;
		if (Math::IsZero(abs(d.x)))
		{
			if (start.x < box.mMin.x || start.x > box.mMax.x)
			{
				return false;
			}
		}
		else
		{
			float ood = 1.0f / d.x;
			float t1 = (box.mMin.x - start.x) * ood;
			float t2 = (box.mMax.x - start.x) * ood;
			if (t1 > t2)
			{
				float temp = t1;
				t1 = t2;
				t2 = t1;
			}
			if (t1 > tmin)
			{
				tmin = t1;
			}
			if (t2 < tmax)
			{  
				tmax = t2;
			}
			if (tmin > tmax)
			{
				return false;
			}
		}
		if (Math::IsZero(abs(d.y)))
		{
			if (start.y < box.mMin.y || start.y > box.mMax.y)
			{
				return false;
			}
		}
		else
		{
			float ood = 1.0f / d.y;
			float t1 = (box.mMin.y - start.y) * ood;
			float t2 = (box.mMax.y - start.y) * ood;
			if (t1 > t2)
			{
				float temp = t1;
				t1 = t2;
				t2 = t1;
			}
			if (t1 > tmin)
			{
				tmin = t1;
			}
			if (t2 < tmax)
			{
				tmax = t2;
			}
			if (tmin > tmax)
			{
				return false;
			}
		}
		if (Math::IsZero(abs(d.z)))
		{
			if (start.z < box.mMin.z || start.z > box.mMax.z)
			{
				return false;
			}
		}
		else
		{
			float ood = 1.0f / d.z;
			float t1 = (box.mMin.z - start.z) * ood;
			float t2 = (box.mMax.z - start.z) * ood;
			if (t1 > t2)
			{
				float temp = t1;
				t1 = t2;
				t2 = t1;
			}
			if (t1 > tmin)
			{
				tmin = t1;
			}
			if (t2 < tmax)
			{
				tmax = t2;
			}
			if (tmin > tmax)
			{
				return false;
			}
			
		}
		outPoint = start + d * tmin;
		if (outPoint.x > segment.mEnd.x && outPoint.y > segment.mEnd.y && outPoint.z > segment.mEnd.z)
		{
			return false;
		}

		return true;
	}

} // namespace
