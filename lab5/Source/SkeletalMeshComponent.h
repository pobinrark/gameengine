#pragma once
#include "MeshComponent.h"
#include "MatrixPalette.h"
#include "Skeleton.h"
#include "Animation.h"

class SkeletalMeshComponent : public MeshComponent
{
	DECL_COMPONENT(SkeletalMeshComponent, MeshComponent);
public:

	struct AnimationStruct {
		AnimationPtr anim;
		float animationPlayRate;
		float animationTime;
	};

	SkeletalMeshComponent(Actor& owner);

	void Draw(class Renderer& render) override;

	void SetSkeleton(SkeletonPtr skeleton) { mSkeleton = skeleton; }
	void Tick(float deltaTime) override;

	// Play an animation. Returns the length of the animation
	float PlayAnimation(AnimationPtr anim, float playRate = 1.0f, float blendTime = 0.0f);

	void SetProperties(const rapidjson::Value& properties) override;
protected:
	void ComputeMatrixPalette(AnimationStruct prev, AnimationStruct current);

	MatrixPalette mPalette;
	SkeletonPtr mSkeleton;
	
	AnimationStruct prevAnimation;
	AnimationStruct currentAnimation;
	
	float mBlendTime;
	float currentBlendTime;

	
};

DECL_PTR(SkeletalMeshComponent);
