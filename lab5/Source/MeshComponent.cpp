#include "ITPEnginePCH.h"
#include <string.h>
IMPL_COMPONENT(MeshComponent, DrawComponent, 1024);

MeshComponent::MeshComponent(Actor& owner)
	:Super(owner)
	,mTextureIndex(0)
{

}

void MeshComponent::Draw(class Renderer& render)
{
	if (mMesh)
	{
		render.DrawMesh(mMesh->GetVertexArray(), mMesh->GetTexture(mTextureIndex),
			mOwner.GetWorldTransform(), mMesh->GetShaderType());
	}
}

void MeshComponent::SetProperties(const rapidjson::Value& properties)
{
	Super::SetProperties(properties);

	// TODO: Lab 3
	std::string mesh;
	int textureIndex;
	if (GetStringFromJSON(properties, "mesh", mesh))
	{
		mMesh = mOwner.GetGame().GetAssetCache().Load<Mesh>(mesh);
		SetMesh(mMesh);
	}
	//const rapidjson::Value& index = properties["textureindex"];
	if (GetIntFromJSON(properties, "textureIndex", textureIndex))
	{
		SetTextureIndex(textureIndex);
	}
}
