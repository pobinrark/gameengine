#include "ITPEnginePCH.h"

IMPL_COMPONENT(SkeletalMeshComponent, MeshComponent, 32);

SkeletalMeshComponent::SkeletalMeshComponent(Actor& owner)
	:MeshComponent(owner)
{
	mBlendTime = 0.5;
}

void SkeletalMeshComponent::Draw(class Renderer& render)
{
	// TODO: Lab 4
	if (mSkeleton)
	{
		render.DrawSkeletalMesh(mMesh->GetVertexArray(), mMesh->GetTexture(mTextureIndex), mOwner.GetWorldTransform(), mPalette);
	}
}

void SkeletalMeshComponent::Tick(float deltaTime)
{
	Super::Tick(deltaTime);

	// TODO: Lab 4
	currentBlendTime += deltaTime;
	if (currentBlendTime > mBlendTime)
	{
		prevAnimation.anim = nullptr;
	}
	if (prevAnimation.anim != nullptr)
	{
		prevAnimation.animationTime = prevAnimation.animationTime + deltaTime * prevAnimation.animationPlayRate;
		if (prevAnimation.animationTime > prevAnimation.anim->GetLength())
		{
			prevAnimation.animationTime -= prevAnimation.anim->GetLength();
		}
	}
	if (currentAnimation.anim != nullptr)
	{
		currentAnimation.animationTime = currentAnimation.animationTime + deltaTime * currentAnimation.animationPlayRate;
		if (currentAnimation.animationTime > currentAnimation.anim->GetLength())
		{
			currentAnimation.animationTime -= currentAnimation.anim->GetLength();
		}
		ComputeMatrixPalette(prevAnimation, currentAnimation);
	}
}

float SkeletalMeshComponent::PlayAnimation(AnimationPtr anim, float playRate /*= 1.0f*/, float blendTime /*= 0.0f*/)
{
	DbgAssert(mSkeleton != nullptr, "Can't play an animation without a skeleton!");
	DbgAssert(mSkeleton->GetNumBones() == anim->GetNumBones(), 
		"Skeleton and animation have a different number of bones!");

	// TODO: Lab 4
	prevAnimation = currentAnimation;
	
	currentAnimation.anim = anim;
	currentAnimation.animationPlayRate = playRate;
	currentAnimation.animationTime = 0.0f;
	currentBlendTime = 0.0f;

	ComputeMatrixPalette(prevAnimation, currentAnimation);
	

	return currentAnimation.anim->GetLength();
}

void SkeletalMeshComponent::SetProperties(const rapidjson::Value& properties)
{
	Super::SetProperties(properties);

	std::string skeleton;
	if (GetStringFromJSON(properties, "skeleton", skeleton))
	{
		mSkeleton = mOwner.GetGame().GetAssetCache().Load<Skeleton>(skeleton);
	}
}

void SkeletalMeshComponent::ComputeMatrixPalette(AnimationStruct prev, AnimationStruct current)
{
	// TODO: Lab 4
	std::vector<Matrix4> outPoses;
	std::vector<Matrix4> newPosesLocal, newPosesGlobal;
	std::vector<BoneTransform> prevBoneT, currentBoneT, newBoneT;

	//get local pose of the bones
	
	current.anim->GetGlobalPoseAtTime(outPoses, mSkeleton, current.animationTime);
	if (prev.anim == nullptr) {
		for (int i = 0; i < mSkeleton->GetNumBones(); i++)
		{
			mPalette.matrixPalette[i] = mSkeleton->GetGlobalInvBindPoses()[i] * outPoses[i];
		}
	}
	else if(prev.anim != nullptr)
	{
		prevBoneT = prev.anim->GetLocalPoseAtTime(prevBoneT, mSkeleton, prev.animationTime);
		currentBoneT = current.anim->GetLocalPoseAtTime(currentBoneT, mSkeleton, current.animationTime);
		for (int i = 0; i < mSkeleton->GetNumBones(); i++)
		{
			newBoneT.push_back(Interpolate(prevBoneT[i], currentBoneT[i], currentBlendTime/mBlendTime));
			//newBoneT.push_back(currentBoneT[i]);
		}
		for (int i = 0; i < mSkeleton->GetNumBones(); i++)
		{
			newPosesLocal.push_back(newBoneT[i].ToMatrix());
		}
		//converting to global space
		//current.anim->GetGlobalPoseAtTime(outPoses, mSkeleton, current.animationTime);
		for (int i = 0; i < mSkeleton->GetNumBones(); i++)
		{
			if (i == 0)
			{
				newPosesGlobal.push_back(newPosesLocal[i]);
			}
			else
			{
				newPosesGlobal.push_back(newPosesLocal[i] * newPosesGlobal.at(mSkeleton->GetBone(i).mParent));
			}
		}

		//mAnimation->GetGlobalPoseAtTime(outPoses, mSkeleton, mAnimationTime);
		for (int i = 0; i < mSkeleton->GetNumBones(); i++)
		{
			mPalette.matrixPalette[i] = mSkeleton->GetGlobalInvBindPoses()[i] * newPosesGlobal[i];
		}
	}
}
