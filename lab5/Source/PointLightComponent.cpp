#include "ITPEnginePCH.h"

IMPL_COMPONENT(PointLightComponent, Component, MAX_POINT_LIGHTS);

PointLightComponent::PointLightComponent(Actor& owner)
	:Component(owner)
{
	// TODO: Lab 3
	mData.position = owner.GetPosition();
	mData.isEnabled = true;
}

void PointLightComponent::Register()
{
	Super::Register();
	mOwner.GetGame().GetRenderer().AddPointLight(ThisPtr());
}

void PointLightComponent::Unregister()
{
	Super::Unregister();
	mOwner.GetGame().GetRenderer().RemovePointLight(ThisPtr());
}

void PointLightComponent::OnUpdatedTransform()
{
	// TODO: Lab 3
	mData.position = GetPosition();
}

void PointLightComponent::SetProperties(const rapidjson::Value& properties)
{
	Super::SetProperties(properties);

	// TODO: Lab 3
	

	Vector3 mDiffuseColor;
	float mInnerRadius;
	float mOuterRadius;
	Vector3 mSpecularColor;
	float mSpecularPower;

	if (GetVectorFromJSON(properties, "diffuseColor", mDiffuseColor))
	{
		SetDiffuseColor(mDiffuseColor);
	}
	if (GetFloatFromJSON(properties, "innerRadius", mInnerRadius))
	{
		SetInnerRadius(mInnerRadius);
	}
	if (GetFloatFromJSON(properties, "outerRadius", mOuterRadius))
	{
		SetOuterRadius(mOuterRadius);
	}
	if (GetVectorFromJSON(properties, "specularColor", mSpecularColor))
	{
		SetSpecularColor(mSpecularColor);
	}
	if (GetFloatFromJSON(properties, "specularPower", mSpecularPower))
	{
		SetSpecularPower(mSpecularPower);
	}
}

