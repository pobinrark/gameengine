#pragma once
#include "Math.h"

const size_t MAX_POINT_LIGHTS = 8;

struct PointLightData
{
	PointLightData();

	// TODO: Lab 3
	Vector3 diffuseColor;
	float padding1;
	Vector3 specularColor;
	float padding2;
	Vector3 position;

	float specularPower;
	//float padding3;
	float innerRadius;
	float outerRadius;

	unsigned int isEnabled;
	float padding3;
};
