#pragma once
#include "Math.h"
#include <xmmintrin.h>
#include <smmintrin.h>

// SHUFFLER is like shuffle, but has easier to understand indices
#define _MM_SHUFFLER( xi, yi, zi, wi ) _MM_SHUFFLE( wi, zi, yi, xi )

class alignas(16) SimdVector3
{
	// Underlying vector
	__m128 mVec;
public:
	// Empty default constructor
	SimdVector3() { }

	// Constructor from __m128
	explicit SimdVector3(__m128 vec)
	{
		mVec = vec;
	}

	// Constructor if converting from Vector3
	explicit SimdVector3(const Vector3& vec)
	{
		FromVector3(vec);
	}

	// Load from a Vector3 into this SimdVector3
	void FromVector3(const Vector3& vec)
	{
		// We can't assume this is aligned
		mVec = _mm_setr_ps(vec.x, vec.y, vec.z, 0.0f);
	}

	// Convert this SimdVector3 to a Vector3
	Vector3 ToVector3() const
	{
		return Vector3(mVec);
	}

	// this = this + other
	void Add(const SimdVector3& other)
	{
		// TODO: Lab 1
		mVec = _mm_add_ps(mVec, other.mVec);
	}

	// this = this - other
	void Sub(const SimdVector3& other)
	{
		// TODO: Lab 1
		mVec = _mm_sub_ps(mVec, other.mVec);

	}

	// this = this * other (componentwise)
	void Mul(const SimdVector3& other)
	{
		// TODO: Lab 1
		mVec = _mm_mul_ps(mVec, other.mVec);
	}

	// this = this * scalar
	void Mul(float scalar)
	{
		// TODO: Lab 1
		__m128 scalarVec = _mm_set_ps1(scalar);
		mVec = _mm_mul_ps(mVec, scalarVec);
	}

	// Normalize this vector
	void Normalize()
	{
		// TODO: Lab 1
		int mask;
		__m128 temp = _mm_dp_ps(mVec, mVec, 0x7f);
		temp = _mm_rsqrt_ps(temp);
		mVec = _mm_mul_ps(temp, mVec);
	}

	// (this dot other), storing the dot product
	// in EVERY COMPONENT of returned SimdVector3
	SimdVector3 Dot(const SimdVector3& other) const
	{
		// TODO: Lab 1
		__m128 temp = _mm_dp_ps(mVec, other.mVec, 0x7f);
		return SimdVector3(temp); // Fix return value
	}

	// Length Squared of this, storing the result in
	// EVERY COMPONENT of returned SimdVector3
	SimdVector3 LengthSq() const
	{
		// TODO: Lab 1
		__m128 temp = _mm_dp_ps(mVec, mVec, 0x7f);

		return SimdVector3(temp); // Fix return value
	}

	// Length of this, storing the result in
	// EVERY COMPONENT of returned SimdVector3
	SimdVector3 Length() const
	{
		// TODO: Lab 1
		__m128 temp = _mm_dp_ps(mVec, mVec, 0x7f);
		temp = _mm_sqrt_ps(temp);
		return SimdVector3(temp); // Fix return value
	}

	// result = this (cross) other
	SimdVector3 Cross(const SimdVector3& other) const
	{
		// TODO: Lab 1
		__m128 tempA = _mm_shuffle_ps(mVec, mVec, _MM_SHUFFLER(1, 2, 0, 3));
		__m128 tempB = _mm_shuffle_ps(other.mVec, other.mVec, _MM_SHUFFLER(2, 0, 1, 3));
		__m128 result = _mm_mul_ps(tempA, tempB);
		tempA = _mm_shuffle_ps(mVec, mVec, _MM_SHUFFLER(2, 0, 1, 3));
		tempB = _mm_shuffle_ps(other.mVec, other.mVec, _MM_SHUFFLER(1, 2, 0, 3));
		tempA = _mm_mul_ps(tempA, tempB);
		result = _mm_sub_ps(result, tempA);
		return SimdVector3(result); // Fix return value
	}

	// result = this * (1.0f - f) + other * f
	SimdVector3 Lerp(const SimdVector3& other, float f) const
	{
		// TODO: Lab 1
		__m128 temp = _mm_set_ps1(1.0f);
		__m128 temp1 = _mm_set_ps1(f);
		temp = _mm_sub_ps(temp, temp1);
		temp = _mm_mul_ps(mVec, temp);

		__m128 temp2 = _mm_mul_ps(other.mVec, temp1);

		__m128 result = _mm_add_ps(temp, temp2);
		return SimdVector3(result); // Fix return value
	}

	friend SimdVector3 Transform(const SimdVector3& vec, const class SimdMatrix4& mat, float w);
};

class alignas(16) SimdMatrix4
{
	// Four vectors, one for each row
	__m128 mRows[4];
public:
	// Empty default constructor
	SimdMatrix4() { }

	// Constructor from array of four __m128s
	explicit SimdMatrix4(__m128 rows[4])
	{
		memcpy(mRows, rows, sizeof(__m128) * 4);
	}

	// Constructor if converting from Matrix4
	explicit SimdMatrix4(const Matrix4& mat)
	{
		FromMatrix4(mat);
	}

	// Load from a Matrix4 into this SimdMatrix4
	void FromMatrix4(const Matrix4& mat)
	{
		// We can't assume that mat is aligned, so
		// we can't use mm_set_ps
		memcpy(mRows, mat.mat, sizeof(float) * 16);
	}

	// Convert this SimdMatrix4 to a Matrix4
	Matrix4 ToMatrix4()
	{
		return Matrix4(mRows);
	}

	// this = this * other
	void Mul(const SimdMatrix4& other)
	{
		// TODO: Lab 1
		SimdMatrix4 temp = other;
		_MM_TRANSPOSE4_PS(temp.mRows[0], temp.mRows[1], temp.mRows[2], temp.mRows[3]);
		
		__m128 x0 = _mm_dp_ps(mRows[0], temp.mRows[0], 0xF1);
		__m128 y0 = _mm_dp_ps(mRows[0], temp.mRows[1], 0xF2);
		__m128 z0 = _mm_dp_ps(mRows[0], temp.mRows[2], 0xF4);
		__m128 w0 = _mm_dp_ps(mRows[0], temp.mRows[3], 0xF8);
		mRows[0]= _mm_add_ps(x0, y0);
		mRows[0] = _mm_add_ps(mRows[0], z0);
		mRows[0] = _mm_add_ps(mRows[0], w0);

		__m128 x1 = _mm_dp_ps(mRows[1], temp.mRows[0], 0xF1);
		__m128 y1 = _mm_dp_ps(mRows[1], temp.mRows[1], 0xF2);
		__m128 z1 = _mm_dp_ps(mRows[1], temp.mRows[2], 0xF4);
		__m128 w1 = _mm_dp_ps(mRows[1], temp.mRows[3], 0xF8);
		mRows[1] = _mm_add_ps(x1, y1);
		mRows[1] = _mm_add_ps(mRows[1], z1);
		mRows[1] = _mm_add_ps(mRows[1], w1);

		__m128 x2 = _mm_dp_ps(mRows[2], temp.mRows[0], 0xF1);
		__m128 y2 = _mm_dp_ps(mRows[2], temp.mRows[1], 0xF2);
		__m128 z2 = _mm_dp_ps(mRows[2], temp.mRows[2], 0xF4);
		__m128 w2 = _mm_dp_ps(mRows[2], temp.mRows[3], 0xF8);
		mRows[2] = _mm_add_ps(x2, y2);
		mRows[2] = _mm_add_ps(mRows[2], z2);
		mRows[2] = _mm_add_ps(mRows[2], w2);

		__m128 x3 = _mm_dp_ps(mRows[3], temp.mRows[0], 0xF1);
		__m128 y3 = _mm_dp_ps(mRows[3], temp.mRows[1], 0xF2);
		__m128 z3 = _mm_dp_ps(mRows[3], temp.mRows[2], 0xF4);
		__m128 w3 = _mm_dp_ps(mRows[3], temp.mRows[3], 0xF8);
		mRows[3] = _mm_add_ps(x3, y3);
		mRows[3] = _mm_add_ps(mRows[3], z3);
		mRows[3] = _mm_add_ps(mRows[3], w3);
		
	}

	// Transpose this matrix
	void Transpose()
	{
		_MM_TRANSPOSE4_PS(mRows[0], mRows[1], mRows[2], mRows[3]);
	}

	// Loads a Scale matrix into this
	void LoadScale(float scale)
	{
		// scale 0 0 0
		mRows[0] = _mm_set_ss(scale);
		mRows[0] = _mm_shuffle_ps(mRows[0], mRows[0], _MM_SHUFFLE(1, 1, 1, 0));

		// 0 scale 0 0
		mRows[1] = _mm_set_ss(scale);
		mRows[1] = _mm_shuffle_ps(mRows[1], mRows[1], _MM_SHUFFLE(1, 1, 0, 1));

		// 0 0 scale 0
		mRows[2] = _mm_set_ss(scale);
		mRows[2] = _mm_shuffle_ps(mRows[2], mRows[2], _MM_SHUFFLE(1, 0, 1, 1));

		// 0 0 0 1
		mRows[3] = _mm_set_ss(1.0f);
		mRows[3] = _mm_shuffle_ps(mRows[3], mRows[3], _MM_SHUFFLE(0, 1, 1, 1));
	}

	// Loads a rotation about the X axis into this
	void LoadRotationX(float angle)
	{
		// 1 0 0 0
		mRows[0] = _mm_set_ss(1.0f);
		mRows[0] = _mm_shuffle_ps(mRows[0], mRows[0], _MM_SHUFFLE(1, 1, 1, 0));

		float cosTheta = Math::Cos(angle);
		float sinTheta = Math::Sin(angle);

		// 0 cos sin 0
		mRows[1] = _mm_setr_ps(0.0f, cosTheta, sinTheta, 0.0f);

		// 0 -sin cos 0
		mRows[2] = _mm_setr_ps(0.0f, -sinTheta, cosTheta, 0.0f);

		// 0 0 0 1
		mRows[3] = _mm_set_ss(1.0f);
		mRows[3] = _mm_shuffle_ps(mRows[3], mRows[3], _MM_SHUFFLE(0, 1, 1, 1));
	}

	// Loads a rotation about the Y axis into this
	void LoadRotationY(float angle)
	{
		float cosTheta = Math::Cos(angle);
		float sinTheta = Math::Sin(angle);

		// cos 0 -sin 0
		mRows[0] = _mm_setr_ps(cosTheta, 0.0f, -sinTheta, 0.0f);

		// 0 1 0 0
		mRows[1] = _mm_set_ss(1.0f);
		mRows[1] = _mm_shuffle_ps(mRows[1], mRows[1], _MM_SHUFFLER(1, 0, 1, 1));

		// sin 0 cos 0
		mRows[2] = _mm_setr_ps(sinTheta, 0.0f, cosTheta, 0.0f);

		// 0 0 0 1
		mRows[3] = _mm_set_ss(1.0f);
		mRows[3] = _mm_shuffle_ps(mRows[3], mRows[3], _MM_SHUFFLE(0, 1, 1, 1));
	}

	// Loads a rotation about the Z axis into this
	void LoadRotationZ(float angle)
	{
		float cosTheta = Math::Cos(angle);
		float sinTheta = Math::Sin(angle);

		// cos sin 0 0
		mRows[0] = _mm_setr_ps(cosTheta, sinTheta, 0.0f, 0.0f);

		// -sin cos 0 0
		mRows[1] = _mm_setr_ps(-sinTheta, cosTheta, 0.0f, 0.0f);

		// 0 0 1 0
		mRows[2] = _mm_set_ss(1.0f);
		mRows[2] = _mm_shuffle_ps(mRows[2], mRows[2], _MM_SHUFFLER(1, 1, 0, 1));

		// 0 0 0 1
		mRows[3] = _mm_set_ss(1.0f);
		mRows[3] = _mm_shuffle_ps(mRows[3], mRows[3], _MM_SHUFFLE(0, 1, 1, 1));
	}

	// Loads a translation matrix into this
	void LoadTranslation(const Vector3& trans)
	{
		// 1 0 0 0
		mRows[0] = _mm_set_ss(1.0f);
		mRows[0] = _mm_shuffle_ps(mRows[0], mRows[0], _MM_SHUFFLER(0, 1, 1, 1));

		// 0 1 0 0
		mRows[1] = _mm_set_ss(1.0f);
		mRows[1] = _mm_shuffle_ps(mRows[1], mRows[1], _MM_SHUFFLER(1, 0, 1, 1));

		// 0 0 1 0
		mRows[2] = _mm_set_ss(1.0f);
		mRows[2] = _mm_shuffle_ps(mRows[2], mRows[2], _MM_SHUFFLER(1, 1, 0, 1));

		mRows[3] = _mm_setr_ps(trans.x, trans.y, trans.z, 1.0f);
	}

	// Loads a matrix from a quaternion into this
	void LoadFromQuaternion(const Quaternion& quat);

	// Inverts this matrix
	void Invert();

	friend SimdVector3 Transform(const SimdVector3& vec, const class SimdMatrix4& mat, float w);
};

inline SimdVector3 Transform(const SimdVector3& vec, const SimdMatrix4& mat, float w = 1.0f)
{
	// TODO: Lab 1
	SimdMatrix4 temp = mat;
	_MM_TRANSPOSE4_PS(temp.mRows[0], temp.mRows[1], temp.mRows[2], temp.mRows[3]);
	__m128 A = vec.mVec;
	__declspec(align(16)) float values[4] = { 0.0f, 0.0f, 0.0f, w };
	__m128 wComponent = _mm_load_ps(values);
	A = _mm_insert_ps(A, wComponent, 0xF0);
	__m128 row1 = _mm_dp_ps(A, temp.mRows[0], 0xF1);
	__m128 row2 = _mm_dp_ps(A, temp.mRows[1], 0xF2);
	__m128 row3 = _mm_dp_ps(A, temp.mRows[2], 0xF4);
	__m128 row4 = _mm_dp_ps(A, temp.mRows[3], 0xF8);
	row1 = _mm_add_ps(row1, row2);
	row1 = _mm_add_ps(row1, row3);
	row1 = _mm_add_ps(row1, row4);
	return SimdVector3(row1); // Fix return value
}
